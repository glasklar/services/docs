This project is tracking Glasklar's documentation site,
https://docs.glasklar.is/, on which projects hosted by
https://git.glasklar.is/ can publish using [hugo](https://gohugo.io/).
